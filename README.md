# DevOps Labs - Java Getting Started

This is a hands on DevOps Java lab where you will learn how to build a simple Java app in a GitLab pipeline with unit testing and code coverage. Step by step slides are available [here](https://nginscale.gitlab.io/training/getting-started-2/#/4).

## Prerequisites

You will need a local development environment with the following tools installed:
    - decent code editor or IDE (Visual Studio Code preferred)
    - Git
    - Docker

## How to Build
